### Запустить тесты
`nosetest tests/`

`nosetest tests/regress_tests/param_tests.py`

`nosetests -vv tests/regress_tests/param_tests.py:test_add`

Что бы запустить как скрипт надо добавить в конце модуля:

```
if __name__ == "__main__":
    import nose
    nose.runmodule(__name__)
```

### Задать уровень вывода

`nosetest -v tests/`

`nosetest -vv tests/`

`nosetest -vvv tests/`

### Аттрибуты

Что бы выполнить только тесты, в которых аттрибут `prior` равен `critical`:

`nosetests -vv -a 'prior=critical' tests/`

Тесты, с аттрибутом `unstable`:
`nosetests -vv -a 'unstable' tests/`

Все тесты, кроме тестов с аттрибутом `unstable`:
`nosetests -vv -a '!unstable' tests/`

### Отчёты
`nosetests -vv --with-xunit tests/`

Задать путь к файлу отчёта
`nosetests -vv --with-xunit --xunit-file='report/result.xml' tests/ `

Задать имя
`nosetests -vv --with-xunit  --xunit-testsuite-name='TestSuite name' tests/`

### Плагины
В `param_tests.py`  используется плагин для параметризованных тестов. Как ставить и инструкция [тут](https://pypi.python.org/pypi/nose-parameterized/).

