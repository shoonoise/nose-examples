from nose.tools import assert_equal
from nose_parameterized import parameterized

par = [
    (2, 3, 5),
    (3, 5, 8),
    (2, 2, 5)
]


@parameterized(par)
def test_add(a, b, expected):
    assert_equal(a + b, expected)


@parameterized.expand([("2 and 3", 2, 3, 5),
                       ("3 and 5", 2, 5, 8)])
def test_add_(_, a, b, expected):
    assert_equal(a + b, expected)
