# coding=utf-8
import logging
from nose.plugins.attrib import attr

logger = logging.getLogger(__name__)


class SomeCustomException(Exception):
    pass


def some_step(number):
    if number < 0:
        raise SomeCustomException("Current error caused by blah blah blah")
    else:
        pass


class TestA():

    def test1(self):
        """
        Имя теста 1
        Описание
        """
        logger.info("I'm in test 1")
        assert 1 == 1

    @attr(prior='critical')
    def test2(self):
        """
        Проверка какой то критичной функциональности
        Тут можно описать технические детали исполнения
        """
        logger.info("I'm in test 2")
        assert 2 == 2

    @attr(prior='minor')
    @attr("unstable")
    def test3(self):
        """
        Этот тест должен упасть
        """
        logger.info("I'm in test 3")
        some_step(-1)
        assert 3 == 3


class TestB():

    @attr("stable")
    def test1(self):
        logger.info("I'm in test 1")
        assert 1 == 1

    @attr(prior='1')
    def test2(self):
        logger.info("I'm in test 2")
        assert 2 == 1, "Two is not equal to one!"
